//Creacion de servidor con codigo NODE

/*
const http = require('http');

http.createServer((req, res) => {
    res.end('Hello world');
}).listen(3000);

*/

//Creacion de servidor con codigo EXPRESS
const express = require('express');
const morgan = require('morgan');
const app = express();

app.use(morgan('combined'));

/*app.use((req, res, next)=>{
    console.log("Primer middleware");
    next();
});*/

app.get('/', (req, res) => {
    res.end('Mensaje respuesta del servidor');
});

app.get('/login', (req, res) => {
    res.end("Logueate al sistema");
});

app.get('*', (req, res) => {
    res.end("Elemento no encontrado");
});

app.listen(3000, () => {
    console.log("Servidor escuchando...");
});