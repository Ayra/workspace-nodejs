const math = require('./math.js');

console.log("8 + 4 = " + math.sumar(8, 4));
console.log("8 - 4 = " + math.restar(8, 4));
console.log("8 * 4 = " + math.multiplicar(8, 4));
console.log("8 / 0 = " + math.dividir(8, 0));