function sumar(a, b){
    return a + b;
}

function restar(a, b){
    return a - b;
}

function multiplicar(a, b){
    return a * b;
}

function dividir(a, b){
    if(b == 0) return "El denominador no puede ser cero";
    else return a / b;
}

var operacion = {};

operacion.sumar = sumar;
operacion.restar = restar;
operacion.dividir = dividir;
operacion.multiplicar = multiplicar;

module.exports = operacion;