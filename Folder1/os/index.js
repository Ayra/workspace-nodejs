const os = require('os');

console.log('Architecture: '+os.arch());
//console.log('Constantes de OS module: ' + os.constants);
//console.log(os.cpus());
console.log(os.endianness());
console.log('Memoria libre: ', os.freemem());
console.log('Ubicacion: ', os.homedir());
console.log('Hostname: ', os.hostname());
//console.log('redes: ',os.networkInterfaces());
console.log('Plataforma: ', os.platform());
console.log(os.release());
console.log(os.type());